import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BidderWizardOptionalFieldsComponent } from './bidder-wizard-optional-fields.component';

describe('BidderWizardOptionalFieldsComponent', () => {
  let component: BidderWizardOptionalFieldsComponent;
  let fixture: ComponentFixture<BidderWizardOptionalFieldsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BidderWizardOptionalFieldsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BidderWizardOptionalFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
