import { Component,AfterViewInit, OnInit, Renderer2, ViewChild, ElementRef }   from '@angular/core';
import { Router }              from '@angular/router';

import { FormDataService }     from '../data/formData.service';


@Component({
  selector: 'app-bidder-wizard-optional-fields',
  templateUrl: './bidder-wizard-optional-fields.component.html',
  styleUrls: ['./bidder-wizard-optional-fields.component.css']
})
export class BidderWizardOptionalFieldsComponent implements OnInit {

  @ViewChild('formField') el:ElementRef;
  @ViewChild('option') op:ElementRef;

  title = 'Bidder optional field creation';
  workType: any;
  workForm: any;
  form: any;
  fieldType: any;
  isSelected: any = false;
  isShowDelete: number = 1;
  invalidLabel: boolean = false;
  public fieldArray = new Array();
  
  choices = Object.assign([], [{'id' : 'option 1', 'name' : 'option2', 'value': ''}]);
  selectBoxes = Object.assign([], [{'id' : 'checkbox 1', 'name' : 'checkbox1', 'value': ''}]);
  
  bidderWizardFormFields = [];

  constructor(private router: Router, public formDataService: FormDataService, private p1:Renderer2) { }

  ngOnInit() {
    this.workType = this.formDataService.getWizardOptionalField();
    this.choices;
    this.selectBoxes;
    this.formDataService.bidderWizardFormFields;
    this.formDataService.bidderWizardSelectedFormFields;
  }


  addOption(){
      var newOptionNo = this.choices.length+1;
      var newOptionValue = this.choices.length+1;
      this.choices.push(Object.assign({}, {'id' : 'option ' + newOptionNo, 'name' : 'option' + newOptionNo, 'value': ''}));
  }

  addLabel(){
      if(this.selectBoxes.length > 3){
          return false;
      }
      var newLabelNo = this.selectBoxes.length+1;
      var newLabelValue = this.selectBoxes.length+1;
      this.selectBoxes.push(Object.assign({}, {'id' : 'checkbox ' + newLabelNo, 'name' : 'checkbox' + newLabelNo, 'value': ''}));
  }


  removeOption = function(id) {
    var newItemNo = this.choices.id;
    if ( newItemNo !== 0 ) {
     this.choices.pop(id);
    }
  };

  removeLabel = function(id) {
    var newselectNo = this.selectBoxes.id;
    if ( newselectNo !== 0 ) {
     this.selectBoxes.pop(id);
    }
  };

  removeField(i){
    // document.getElementById(i).remove();
    this.formDataService.bidderWizardFormFields.splice(i, 1);
  }


  createDiv(value){
      if(value.required == true){
          var required = 'true';
      }else{
          var required = 'false';
      }

      let outrDiv = this.p1.createElement('div');  
          outrDiv.setAttribute("class", 'form-group inr_pd');  
      let label = this.p1.createElement('label'); 
          label.setAttribute("class", 'control-label" for="'+value.label.toLowerCase()+'');
          label.setAttribute("value", value.label);  
      let innerDiv = this.p1.createElement('div');  
          innerDiv.setAttribute("class", 'row_div');
      let text = this.p1.createText(value.label); 
      let input = this.p1.createElement('input'); 
          if(value.fieldType == 'checkbox'){
              input.setAttribute("class", 'form-group input-md');
          }else{
              input.setAttribute("class", 'form-control input-md');
          }
          
          if(value.fieldType != 'file' || value.fieldType != 'date' || value.fieldType != 'time' || value.fieldType != 'number'){
              input.setAttribute("maxlength", value.length);
          }else if(value.fieldType != 'number'){
              input.setAttribute("min", 1);
          }
          input.setAttribute("disabled", true);
          input.setAttribute("ng-required", required);
          input.setAttribute("id", value.label.toLowerCase());
          input.setAttribute("name", value.label.toLowerCase());
          input.setAttribute("type", value.fieldType);
          input.setAttribute("ngModel", 'workForm.'+value.label.toLowerCase()+'');
      let btn = this.p1.createElement("button");
          btn.setAttribute("class", 'btn btn-sm btn-del btn-danger');
          btn.setAttribute("id", 'delBtn');
      let icon = this.p1.createElement("i");
          icon.setAttribute("class", 'fa fa-trash');
      this.p1.listen(btn, "click", () => this.p1.removeChild(this.el.nativeElement, outrDiv));

      this.p1.appendChild(outrDiv, label);
      this.p1.appendChild(label, text);
      this.p1.appendChild(outrDiv, innerDiv);
      this.p1.appendChild(innerDiv, input);
      this.p1.appendChild(innerDiv, btn);
      this.p1.appendChild(btn, icon);

      this.p1.appendChild(this.el.nativeElement, outrDiv);

      return true;
  }

  check_is_valid_label(value){

      var isPresent = this.formDataService.bidderWizardFormFields.some(function(el){ return el.label === value});
      if(isPresent === true){
          this.invalidLabel = true;
          return false;
      }else{
          this.invalidLabel = false;
      }
      
  }

  titleCase(str) {
      var splitStr = str.toLowerCase().split(' ');
      for (var i = 0; i < splitStr.length; i++) {
          // You do not need to check if i is larger than splitStr length, as your for does that for you
          // Assign it back to the array
          splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);     
      }
      // Directly return the joined string
      return splitStr.join(' '); 
  }

  nameCase(str) {
      var splitStr = str.toLowerCase().split(' ');
      // for (var i = 0; i < splitStr.length; i++) {
      //     // You do not need to check if i is larger than splitStr length, as your for does that for you
      //     // Assign it back to the array
      //     splitStr[i] = splitStr[i].charAt(0).toLowerCase() + splitStr[i].substring(1);     
      // }
      // Directly return the joined string
      return splitStr.join(' '); 
  }





  submitInner(value) {
      //create the DOM element 
      let valueData = value
      var label = this.titleCase(valueData.label);
      
      var isPresent = this.formDataService.bidderWizardFormFields.some(function(el){ return el.label === label});
      if(isPresent === true){
          this.invalidLabel = true;
          return false;
      }
    
      valueData.label = label;
      valueData.fieldName = this.nameCase(valueData.label).trim().replace(/\s/g, "_");
      valueData.wizard_required = "no";

      if(value.fieldType == 'dropdown'){

        var i = 0;
        for(let choice of this.choices) {
        this.choices[i]['value'] = this.titleCase(choice.value);
        i++;
        }
        valueData.options = Object.assign([],this.choices);

      }else if(value.fieldType == 'checkbox'){
        var j = 0;
        for(let select of this.selectBoxes) {
          this.selectBoxes[j]['value'] = this.titleCase(select.value);
          j++;
        }

        valueData.selectLabels = Object.assign([],this.selectBoxes);
      }
      this.bidderWizardFormFields.push(Object.assign({}, valueData));
      this.formDataService.bidderWizardFormFields.push(Object.assign({}, valueData));
      this.choices = Object.assign([], [{'id' : 'option 1', 'name' : 'option2', 'value': ''}]);
      this.workType = this.formDataService.getWizardOptionalField();
      this.selectBoxes = Object.assign([], [{'id' : 'checkbox 1', 'name' : 'checkbox1', 'value': ''}]);
      this.isSelected = false;
  }



  save(form: any): boolean {
      // if (!form.valid) {
      //     return false;
      // }
      this.formDataService.setWizardOptionalField(this.workType);
      return true;
  }

  goToPrevious(form: any) {
      if (this.save(form)) {
          // Navigate to the personal page
          this.router.navigate(['/personal']);
      }
  }


  SelectedRow(filterVal: any) {
    this.fieldType = filterVal;
    this.isSelected = true;
  }

  goToNext(form: any) {
      if (this.save(form)) {
          this.router.navigate(['/bidder_wizard_form_review']);
      }
  }

}
