import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef }   from '@angular/core';
import { Router }              from '@angular/router';

import { Personal }            from '../data/formData.model';
import { FormDataService }     from '../data/formData.service';
import { ApiService }     from '../api.service';
import { AlertService} from '../alert.service';
import { BidderWizardWorkflowService} from '../bidder-wizard-workflow/bidder-wizard-workflow.service';
import { BIDDERWIZARDSTEPS }              from '../bidder-wizard-workflow/bidder-wizard-workflow.model';
import { isNullOrUndefined, EmitType } from '@syncfusion/ej2-base';

@Component({
  selector: 'app-bidder-wizard-required-fields',
  templateUrl: './bidder-wizard-required-fields.component.html',
  styleUrls: ['./bidder-wizard-required-fields.component.css']
})
export class BidderWizardRequiredFieldsComponent implements OnInit {

  constructor(
    private router: Router,
    private formDataService: FormDataService, 
    private apiService: ApiService, 
    private alertService: AlertService,
    private bidderWizardWorkflowService: BidderWizardWorkflowService,
    
  ) { }

  

  ngOnInit() {
    // this.categoryField.nativeElement.focus();
    // this.personal = this.formDataService.getPersonal();
  }


  save(form: any): boolean {
      if (!form.valid) {
          return false;
      }
      //this.formDataService.setPersonal(this.personal);
      return true;
  }

  goToNext() {
          // Navigate to the work page
          this.router.navigate(['/bidder_wizard_optional_fields']);
          this.bidderWizardWorkflowService.validateStep(BIDDERWIZARDSTEPS.required);

  }

}
