import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BidderWizardRequiredFieldsComponent } from './bidder-wizard-required-fields.component';

describe('BidderWizardRequiredFieldsComponent', () => {
  let component: BidderWizardRequiredFieldsComponent;
  let fixture: ComponentFixture<BidderWizardRequiredFieldsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BidderWizardRequiredFieldsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BidderWizardRequiredFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
