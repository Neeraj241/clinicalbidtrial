import { Component,AfterViewInit, Input, OnInit, Renderer2, ViewChild, ElementRef }   from '@angular/core';
import { Router }              from '@angular/router';

import { FormData }                   from '../data/formData.model';
import { FormDataService }            from '../data/formData.service';
import { ApiService }            from '../api.service';
import { AlertService } from '../alert.service';
import { Personal }            from '../data/formData.model';

@Component({
  selector: 'app-bidder-wizard-form-review',
  templateUrl: './bidder-wizard-form-review.component.html',
  styleUrls: ['./bidder-wizard-form-review.component.css']
})
export class BidderWizardFormReviewComponent implements OnInit {
  title = 'Thanks for staying tuned!';
  @Input() formData: FormData;
  isFormValid: boolean = false;
  personal: Personal;

  @ViewChild('formField') el:ElementRef;
  @ViewChild('option') op:ElementRef;

  constructor(
    private router: Router, 
    private formDataService: FormDataService, 
    private alertService: AlertService,
    private p1:Renderer2, 
    private apiService: ApiService
  ) { }


  createDiv(value){
    if(value.required == true){
        var required = 'true';
    }else{
        var required = 'false';
    }

    let outrDiv = this.p1.createElement('div');  
        outrDiv.setAttribute("class", 'form-group ');  
    let label = this.p1.createElement('label'); 
        label.setAttribute("class", 'control-label" for="'+value.label.toLowerCase()+'');
        label.setAttribute("value", value.label);  
    let innerDiv = this.p1.createElement('div');  
        innerDiv.setAttribute("class", 'row_div');
    let text = this.p1.createText(value.label); 
    let input = this.p1.createElement('input'); 
    let requiredSpan = this.p1.createElement('span');
    requiredSpan.setAttribute("class", "text-red"); 
    let requiredtext = this.p1.createText(' *'); 

        if(value.fieldType == 'checkbox'){
            input.setAttribute("class", 'form-control input-md');
        }else{
            input.setAttribute("class", 'form-control input-md');
        }
        
        if(value.fieldType != 'file' || value.fieldType != 'date' || value.fieldType != 'time' || value.fieldType != 'number'){
            input.setAttribute("maxlength", value.length);
        }else if(value.fieldType != 'number'){
            input.setAttribute("min", 1);
        }
        input.setAttribute("disabled", true);
        input.setAttribute("ng-required", required);
        input.setAttribute("id", value.label.toLowerCase());
        input.setAttribute("name", value.label.toLowerCase());
        input.setAttribute("type", value.fieldType);
        input.setAttribute("ngModel", 'workForm.'+value.label.toLowerCase()+'');


    this.p1.appendChild(outrDiv, label);
    this.p1.appendChild(label, text);
    if(value.required == true){
        this.p1.appendChild(label, requiredSpan);
        this.p1.appendChild(requiredSpan, requiredtext);
    }
    this.p1.appendChild(outrDiv, innerDiv);
    this.p1.appendChild(innerDiv, input);


    this.p1.appendChild(this.el.nativeElement, outrDiv);

    return true;
  }


  ngAfterViewInit(){

      var formFields = this.formDataService.bidderWizardFormFields;

      console.log(formFields);

      this.formDataService.bidderWizardFormFields.forEach((value) => {
          if(value.required == true){
              var required = 'true';
          }else{
              var required = 'false';
          }
          //create text for the element
          var i = 1;
          
          switch (value.fieldType) {
              
              case 'text':
                  this.createDiv(value);
                  break;
              case 'email':
                  this.createDiv(value);
                  break;
              case 'number':
                  this.createDiv(value);
                  break;
              case 'textarea':
                  let textAreaOutrDiv = this.p1.createElement('div');  
                      textAreaOutrDiv.setAttribute("class", 'form-group');  
                  let textAreaLabel = this.p1.createElement('label'); 
                      textAreaLabel.setAttribute("class", 'control-label" for="'+value.label.toLowerCase()+'');
                      textAreaLabel.setAttribute("value", value.label);  
                  let textAreaInnerDiv = this.p1.createElement('div');  
                      textAreaInnerDiv.setAttribute("class", 'row_div');
                  let textAreaText = this.p1.createText(value.label); 
                  let textAreaInput = this.p1.createElement('textarea'); 
                      textAreaInput.setAttribute("class", 'form-control input-md');
                      textAreaInput.setAttribute("rows", value.length);
                      textAreaInput.setAttribute("ng-required", required);
                      textAreaInput.setAttribute("disabled", true);
                      textAreaInput.setAttribute("id", value.label.toLowerCase());
                      textAreaInput.setAttribute("name", value.label.toLowerCase());
                      textAreaInput.setAttribute("type", value.fieldType);
                      textAreaInput.setAttribute("ngModel", 'workForm.'+value.label.toLowerCase()+'');

                      let requiredSpan = this.p1.createElement('span');
                      requiredSpan.setAttribute("class", "text-red"); 
                      let requiredtext = this.p1.createText(' *'); 

                  this.p1.appendChild(textAreaOutrDiv, textAreaLabel);
                  this.p1.appendChild(textAreaLabel, textAreaText);
                  if(value.required == true){
                      this.p1.appendChild(textAreaLabel, requiredSpan);
                      this.p1.appendChild(requiredSpan, requiredtext);
                  }
                  this.p1.appendChild(textAreaOutrDiv, textAreaInnerDiv);
                  this.p1.appendChild(textAreaInnerDiv, textAreaInput);

                  this.p1.appendChild(this.el.nativeElement, textAreaOutrDiv)
                  break;
              case 'date':
                  this.createDiv(value);  
                  break;
              case 'time':
                  this.createDiv(value);
                  break;  
              case 'checkbox':
              let checkboxOutrDiv = this.p1.createElement('div');  
                  checkboxOutrDiv.setAttribute("class", 'form-group ');  
              let checkboxLabel = this.p1.createElement('label'); 
              checkboxLabel.setAttribute("class", 'control-label" for="'+value.label.toLowerCase()+'');
              checkboxLabel.setAttribute("value", value.label);  
              let checkboxInnerDiv = this.p1.createElement('div');  
              checkboxInnerDiv.setAttribute("class", 'row_div');
              let checkboxText = this.p1.createText(value.label); 
          

              this.p1.appendChild(checkboxOutrDiv, checkboxLabel);
              this.p1.appendChild(checkboxLabel, checkboxText);
              this.p1.appendChild(checkboxOutrDiv, checkboxInnerDiv);

                  var checkboxes = value.selectLabels;
                  let j = 1;
                  var option = "selectOption"+j;
                  var select  = "selectOptionText"+j;
                  for(let checkbox of checkboxes) {
                      let boxDiv = this.p1.createElement('div');
                      boxDiv.setAttribute("style", 'margin-right:10px');
                      let boxLabel = this.p1.createElement('label');
                          boxLabel.setAttribute("style", 'margin-right:10px');
                          boxLabel.setAttribute("class", 'control-label');
                      let boxLabelText = this.p1.createText(checkbox.value);
                      let checkboxInput = this.p1.createElement('input'); 
                          checkboxInput.setAttribute("class", 'form-group input-md');
                          checkboxInput.setAttribute("disabled", true);
                          checkboxInput.setAttribute("id", checkbox.id.toLowerCase());
                          checkboxInput.setAttribute("name", checkbox.name.toLowerCase());
                          checkboxInput.setAttribute("type", value.fieldType);
                          checkboxInput.setAttribute("ngModel", 'workForm.'+checkbox.name.toLowerCase()+'');
                      //let select = this.p1.createText(choice.value); 
                      //this.p1.appendChild(selectInput, option);

                      let checkrequiredSpan = this.p1.createElement('span');
                      checkrequiredSpan.setAttribute("class", "text-red"); 
                      let checkrequiredtext = this.p1.createText(' *'); 

                      this.p1.appendChild(checkboxInnerDiv, boxDiv);
                      this.p1.appendChild(boxDiv, boxLabel);
                      if(value.required == true){
                          this.p1.appendChild(boxLabel, checkrequiredSpan);
                          this.p1.appendChild(checkrequiredSpan, checkrequiredtext);
                      }
                      this.p1.appendChild(boxLabel, boxLabelText);
                      this.p1.appendChild(boxDiv, checkboxInput);
                      j++;            
                  }

          
                  this.p1.appendChild(this.el.nativeElement, checkboxOutrDiv);
                  break;
              case 'switchbutoon':
                  let outrDiv = this.p1.createElement('div');  
                      outrDiv.setAttribute("class", 'form-group');  
                  let label = this.p1.createElement('label'); 
                      label.setAttribute("class", 'control-label" for="'+value.label.toLowerCase()+'');
                      label.setAttribute("value", value.label);  
                  let innerDiv = this.p1.createElement('div');  
                      innerDiv.setAttribute("class", 'row_div');
                  let text = this.p1.createText(value.label); 
                  let switchlabel = this.p1.createElement('label');
                      switchlabel.setAttribute("class", 'switch');
                  let input = this.p1.createElement('input'); 
                      input.setAttribute("class", 'form-control input-md');
                      input.setAttribute("id", value.label.toLowerCase());
                      input.setAttribute("name", value.label.toLowerCase());
                      input.setAttribute("type", 'checkbox');
                  let switchspan = this.p1.createElement('span');
                      switchspan.setAttribute("class", 'slider round');

                  let switchrequiredSpan = this.p1.createElement('span');
                  switchrequiredSpan.setAttribute("class", "text-red"); 
                  let switchrequiredtext = this.p1.createText(' *'); 

                  this.p1.appendChild(outrDiv, label);
                  this.p1.appendChild(label, text);
                  if(value.required == true){
                      this.p1.appendChild(label, switchrequiredSpan);
                      this.p1.appendChild(switchrequiredSpan, switchrequiredtext);
                  }
                  this.p1.appendChild(outrDiv, innerDiv);
                  this.p1.appendChild(innerDiv, switchlabel);
                  this.p1.appendChild(switchlabel, input);
                  this.p1.appendChild(switchlabel, switchspan);

                  this.p1.appendChild(this.el.nativeElement, outrDiv);

                  return true;
                  break;
              case 'dropdown':
                  var choices = value.options;
                  console.log(choices);
                  let selectAreaOutrDiv = this.p1.createElement('div');  
                      selectAreaOutrDiv.setAttribute("class", 'form-group');  
                  let selectAreaLabel = this.p1.createElement('label'); 
                      selectAreaLabel.setAttribute("class", 'control-label" for="'+value.label.toLowerCase()+'');
                      selectAreaLabel.setAttribute("value", value.label);  
                  let selectInnerDiv = this.p1.createElement('div');  
                      selectInnerDiv.setAttribute("class", 'row_div');
                  let selectText = this.p1.createText(value.label); 
                  let selectInput = this.p1.createElement('select'); 
                      selectInput.setAttribute("class", 'form-control input-md');
                      selectInput.setAttribute("ng-required", required);
                      selectInput.setAttribute("id", value.label.toLowerCase());
                      selectInput.setAttribute("name", value.label.toLowerCase());
                      selectInput.setAttribute("ngModel", 'workForm.'+value.label.toLowerCase()+'');

                      let selectrequiredSpan = this.p1.createElement('span');
                      selectrequiredSpan.setAttribute("class", "text-red"); 
                      let selectrequiredtext = this.p1.createText(' *'); 

                  this.p1.appendChild(selectAreaOutrDiv, selectAreaLabel);
                  this.p1.appendChild(selectAreaLabel, selectText);
                  if(value.required == true){
                      this.p1.appendChild(selectAreaLabel, selectrequiredSpan);
                      this.p1.appendChild(selectrequiredSpan, selectrequiredtext);
                  }
                  this.p1.appendChild(selectAreaOutrDiv, selectInnerDiv);
                  this.p1.appendChild(selectInnerDiv, selectInput);

                  let i = 1;
                  var option = "selectOption"+i;
                  var select  = "selectOptionText"+i;
                  for(let choice of choices) {
                      let option = this.p1.createElement('option');
                      let select = this.p1.createText(choice.value); 
                      this.p1.appendChild(selectInput, option);
                      this.p1.appendChild(option, select);
                      i++;            
                  }

                  this.p1.appendChild(this.el.nativeElement, selectAreaOutrDiv)
                  
                  break;
              case 'file':
                  this.createDiv(value);
                  break;
              default:

          }
      });
          
      // });
  }

  ngOnInit() {
      this.isFormValid = this.formDataService.isFormValid();
      this.personal = this.formDataService.getPersonal();
      this.formDataService.bidderWizardFormFields;
      console.log('Result feature loaded!');
      
  }

  submitForm() {

      this.apiService.saveBidderWizardForm('1',this.personal.categories)
          .subscribe(
              data => {
                var userId = data['data']['user_id'];
                this.formData = this.formDataService.resetFormData();
                this.router.navigate(['/form-list/'+userId]);
              },
              error => {
                  this.alertService.success('Sorry! Some issue occur please try again after some time', true);
              });

  }

  save(form: any): boolean {
      // if (!form.valid) {
      //     return false;
      // }
      return true;
  }




  goToPrevious(form: any) {
      if (this.save(form)) {
          // Navigate to the personal page
          this.router.navigate(['/bidder_wizard_optional_fields']);
      }
  }

}
