import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BidderWizardFormReviewComponent } from './bidder-wizard-form-review.component';

describe('BidderWizardFormReviewComponent', () => {
  let component: BidderWizardFormReviewComponent;
  let fixture: ComponentFixture<BidderWizardFormReviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BidderWizardFormReviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BidderWizardFormReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
