export const BIDDERWIZARDSTEPS = {
    required : 'bidder_wizard_reuired_fields',
    optional: 'bidder_wizard_optional_fields',
    result: 'bidder_wizard_form_review'
}
