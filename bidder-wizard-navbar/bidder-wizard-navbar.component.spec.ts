import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BidderWizardNavbarComponent } from './bidder-wizard-navbar.component';

describe('BidderWizardNavbarComponent', () => {
  let component: BidderWizardNavbarComponent;
  let fixture: ComponentFixture<BidderWizardNavbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BidderWizardNavbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BidderWizardNavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
