import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { FormDataService }     from '../data/formData.service';

@Component({
  selector: 'app-bidder-wizard-navbar',
  templateUrl: './bidder-wizard-navbar.component.html',
  styleUrls: ['./bidder-wizard-navbar.component.css']
})
export class BidderWizardNavbarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
